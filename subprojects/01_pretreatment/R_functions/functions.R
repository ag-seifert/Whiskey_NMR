mds.plot <- function(sum.exp, assay = "data", var.group, method = "abspearson",
                     center = TRUE, scale = TRUE,
                     col = c("blue", "red"), 
                     pch = 19, main = "MDS",
                     return.pc = FALSE, plots.dir = NULL, plot.file = NULL, pos.legend = "topright",cex.legend = 0.75) {
  
  require(amap)
  
  main = paste0(main, " (", method, ")")
  data = assays(sum.exp)[[assay]]
  
  if (center == TRUE) {
    mean = apply(data, 1, mean)
    data = data - mean
  }
  if (scale == TRUE) {
    sd = apply(data, 1, sd)
    data = data / sd
  }
  
  ## distance (absolute 1 - Pearson)
  d = Dist(t(data), method = method)
  fit = cmdscale(d, eig = TRUE, k = 2) # k is the number of dim
  scores = fit$points
  
  ## color
  group = as.data.frame(colData(sum.exp))[, var.group[1]]
  if (!is.factor(group)) group = factor(group)
  names(group) = colnames(sum.exp)
  group = group[rownames(scores)]
  
  ## plotting character
  if (length(var.group) == 2) {
    group.pch = as.data.frame(colData(sum.exp))[, var.group[2]]
    if (!is.factor(group.pch)) group.pch = factor(group.pch)
    names(group.pch) = colnames(sum.exp)
    group.pch = group.pch[rownames(scores)]
    pch.plot = c(17, 19)[as.numeric(group.pch)]
  } else {
    pch.plot = 19
  }
  
  if (is.null(plot.file)) {
    plot.file = file.path(plots.dir,
                          paste("mds_plot_", method, "_", var.group, ".pdf", sep = ""))
  }
  plot(scores[, 1:2], pch = pch.plot,
       main = main,
       col = col[as.numeric(group)],
       xlab = "Coordinate 1",
       ylab = "Coordinate 2")
  legend(pos.legend, legend = levels(group), title = var.group[1],
         col = col, pch = 19, bty = "n", cex = cex.legend)
  if (length(var.group) == 2) {  
    legend("topleft", legend = levels(group.pch), title = var.group[2],
           col = "black", pch = c(17, 19), bty = "n")
  }
  
  if (return.pc) {
    return(scores[, 1:2])
  }
}

autoscale = function(data) {
  data = t(data)
  sd = apply(data, 1, sd)
  mean = apply(data, 1, mean)
  data.auto = (data - mean) / sd
  return(t(data.auto))
}

norm.house = function(data.x) {
  
  metabolite_name = strsplit(colnames(data.x), split = "min")
  min = unlist(lapply(metabolite_name , function(l) l[[1]]))
  mz = unlist(lapply(metabolite_name , function(l) l[[2]]))
  mz.num = rep(NA,length(mz))
  min.num = rep(NA,length(min))
  for (i in 1:length(mz)) {
    mz.num[i] = as.numeric(substring(mz[i],4,10))
    min.num[i] = as.numeric(substring(min[i],2,nchar(min[i])))
  }
  data.house = data.x
  
  data.house[,intersect(which(min.num < 13), which(mz.num  > 700))] = data.x[,intersect(which(min.num < 13), which(mz.num  > 700))] /  data.x[,which(mz.num  ==  732.553)]   # group 1
  data.house[,which(mz.num  ==  732.553)] = data.x[,which(mz.num  ==  732.553)] 
  
  data.house[,intersect(which(min.num > 13), which(mz.num  > 700))] = data.x[,intersect(which(min.num > 13), which(mz.num  > 700))] /  data.x[,which(mz.num  ==  877.726)]   # group 2
  data.house[,which(mz.num  ==  877.726)] = data.x[,which(mz.num  ==  877.726)] 
  
  
  data.house[,intersect(which(min.num < 13), which(mz.num  < 700))] = data.x[,intersect(which(min.num < 13), which(mz.num  < 700))] /  data.x[,which(mz.num  ==  635.464)]   # group 3
  data.house[,which(mz.num  ==  635.464)] = data.x[,which(mz.num  ==  635.464)] 
  
  data.house[,intersect(which(min.num > 13), which(mz.num  < 700))] = data.x[,intersect(which(min.num > 13), which(mz.num  < 700))] /  data.x[,which(mz.num  ==  616.566)]   # group 4
  data.house[,which(mz.num  ==  616.566)] = data.x[,which(mz.num  ==  616.566)] 
  return(data.house)
}

vectornorm = function(y = y) {
  y.sum = sum(y)
  norm = rep(NA,length(y))
  for (i in 1:length(y)) {
    norm[i] =  y[i] / y.sum
  }
  return(norm)
}
